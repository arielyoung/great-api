great-api 

### As a commandline application
A Repository for a project that takes in a multi-char (',', ' ', or '|')  delimited input 
and spits out the input in a requested order.

#### Usage

Must have java 8 installed, I recommend using sdkman to install openjdk8

Compile using `mvn compile`

cd into the `target/classes` folder

run `java young.ariel.DataFeeder <filename`

### Running as a rest api
#### Usage 
Start up the spring boot app: `./mvnw spring-boot:run`

The rest api can be found at `<host>/api/records` 

##### Supported requests
GET `/api/records` - will return all the records from the instance. 

POST `/api/records` with a deliminated one line of data - will create a new Person record. 

GET `/api/records/gender` will return the records ordered by gender(F then M) then by last name (A -> Z).

GET `/api/records/name` will return the records ordered by last name(Z -> A).  

GET `/api/records/birthdate` will return the records ordered by birthdate, ascending.


##### Technologies
- Springboot
- openjdk 8
- junit-jupiter 

#### Decisions
##### Choosing a language
I was torn between using Java and Python but ultimately chose to go with Java because I was more familiar with it in general.
I've written apis in python before, but never a cli that took in parameters. I doubt that it is hard to pick up on, but I wasn't 
willing to spend more time researching than coding. 

##### Choosing a build automation tool
I had previously work with ant but at my current job there were huge efforts of moving projects off of ant. I know ant
gives more leeway with the different targets, but for this project the basics were fine (build, run, test). I hadn't done
may projects in maven nor gradle. But I did do my research and the support for maven was far more than gradle. This was 
ultimately the determining factor in choosing maven. 

##### Choosing Spring-boot 
When I first mapped out the project, I went straight for spring-boot as I knew its potential. It has great annotations that
provide a certain type of "spring magic" making it cleaner for mapping out services and entity classes. It also has a great
built in integration testing by bringing up the app to test against it. It also worked well with my existing cli class to parse
and sort out the data.