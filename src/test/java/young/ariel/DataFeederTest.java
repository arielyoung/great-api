package young.ariel;

import young.ariel.model.Person;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DataFeederTest {
    DataFeeder df;

    @BeforeAll
    private void init() {
        df = new DataFeeder();
    }

    @Test
    public void testInvalidLineFormat() {
        String testLine = "LastName : FirstName : Male : Color : 1/1/1990";
        Person p = df.convertStringToPerson(testLine);
        assertNull(p);
    }

    @Test
    public void testReadCommaLine() {
        String testLine = "LastName, FirstName, Male, Color, 1/1/1990";
        Person p = df.convertStringToPerson(testLine);
        assertNotNull(p);
        assertEquals("LastName", p.getLastName());
        assertEquals("FirstName", p.getFirstName());
        assertEquals("Male", p.getGender());
        assertEquals("Color", p.getFavoriteColor());
        assertEquals("Mon Jan 01 00:00:00 CST 1990", p.getDateOfBirth().toString());
    }

    @Test
    public void testReadPipeLine() {
        String testLine = "LastName | FirstName | Male | Color | 1/1/1990";
        Person p = df.convertStringToPerson(testLine);
        assertNotNull(p);
        assertEquals("LastName", p.getLastName());
        assertEquals("FirstName", p.getFirstName());
        assertEquals("Male", p.getGender());
        assertEquals("Color", p.getFavoriteColor());
        assertEquals("Mon Jan 01 00:00:00 CST 1990", p.getDateOfBirth().toString());
    }

    @Test
    public void testReadSpaceLine() {
        String testLine = "LastName FirstName Male Color 1/1/1990";
        Person p = df.convertStringToPerson(testLine);
        assertNotNull(p);
        assertEquals("LastName", p.getLastName());
        assertEquals("FirstName", p.getFirstName());
        assertEquals("Male", p.getGender());
        assertEquals("Color", p.getFavoriteColor());
        assertEquals("Mon Jan 01 00:00:00 CST 1990", p.getDateOfBirth().toString());
    }

    @Test
    public void testOrderByGender() {
        Person p1 = df.convertStringToPerson("Young Ariel Female Blue 1/28/1995");
        Person p2 = df.convertStringToPerson("Cole Krista Female Green 3/10/1990");
        Person p3 = df.convertStringToPerson("Dom John Male Purple 4/15/1980");
        List<Person> p = Arrays.asList(p1, p2, p3);

        df.outputBy("Gender", p);
        assertEquals(p2, p.get(0));
        assertEquals(p1, p.get(1));
        assertEquals(p3, p.get(2));
    }

    @Test
    public void testOrderByDOB() {
        Person p1 = df.convertStringToPerson("Young Ariel Female Blue 1/28/1995");
        Person p2 = df.convertStringToPerson("Cole Krista Female Green 3/10/1990");
        Person p3 = df.convertStringToPerson("Dom John Male Purple 4/15/1980");
        List<Person> p = Arrays.asList(p1, p2, p3);

        df.outputBy("Birthdate", p);
        assertEquals(p3, p.get(0));
        assertEquals(p2, p.get(1));
        assertEquals(p1, p.get(2));
    }

    @Test
    public void testOrderByInvalidOption() {
        Person p1 = df.convertStringToPerson("Young Ariel Female Blue 1/28/1995");
        Person p2 = df.convertStringToPerson("Cole Krista Female Green 3/10/1990");
        Person p3 = df.convertStringToPerson("Dom John Male Purple 4/15/1980");
        List<Person> p = Arrays.asList(p1, p2, p3);
        try {
            df.outputBy("Color", p);
        } catch (UnsupportedOperationException ignored) {
            assertEquals("Method to sort by not found: Color", ignored.getMessage());
        }
    }

    @Test
    public void testOrderByLastName() {
        Person p1 = df.convertStringToPerson("Young Ariel Female Blue 1/28/1995");
        Person p2 = df.convertStringToPerson("Cole Krista Female Green 3/10/1990");
        Person p3 = df.convertStringToPerson("Dom John Male Purple 4/15/1980");
        List<Person> p = Arrays.asList(p1, p2, p3);

        df.outputBy("Name", p);
        assertEquals(p1, p.get(0));
        assertEquals(p3, p.get(1));
        assertEquals(p2, p.get(2));
    }
}
