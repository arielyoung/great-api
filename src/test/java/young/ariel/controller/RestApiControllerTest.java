package young.ariel.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import young.ariel.GreatApiApplication;
import young.ariel.service.PersonService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GreatApiApplication.class, secure = false)
@ComponentScan(basePackages = "young.ariel")
@AutoConfigureMockMvc
public class RestApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    @Autowired
    private PersonService personService;

    @Test
    public void testGetResponse() {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/api/records").accept(
                MediaType.APPLICATION_JSON);
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
            assertEquals(200, result.getResponse().getStatus());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testPostRequest() {
        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/records")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("Pan Jay Male Orange 1/28/2000");
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
            assertEquals(201, result.getResponse().getStatus());
            assertEquals("{\"id\":1000,\"lastName\":\"Pan\",\"firstName\":\"Jay\",\"gender\":\"Male\",\"favoriteColor\":\"Orange\",\"dateOfBirth\":\"2000-01-28\"}", result.getResponse().getContentAsString());
        } catch (Exception e) {
            fail();
        }

    }
    @Test
    public void testSortByGender() {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/api/records/gender").accept(
                MediaType.APPLICATION_JSON);
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
            assertEquals(200, result.getResponse().getStatus());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testSortByName() {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/api/records/name").accept(
                MediaType.APPLICATION_JSON);
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
            assertEquals(200, result.getResponse().getStatus());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testSortByBirthdate() {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/api/records/birthdate").accept(
                MediaType.APPLICATION_JSON);
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
            assertEquals(200, result.getResponse().getStatus());
        } catch (Exception e) {
            fail();
        }
    }

}
