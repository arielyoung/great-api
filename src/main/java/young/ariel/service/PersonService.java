package young.ariel.service;

import java.util.List;
import young.ariel.model.Person;

public interface PersonService {
    public List<Person> findAllPeople();
    public void addPerson(Person p);
    public Person createPerson(String input);
    public void sort(List<Person> people, String sortBy);

}
