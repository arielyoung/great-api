package young.ariel.service;

import org.springframework.stereotype.Service;
import young.ariel.DataFeeder;
import young.ariel.model.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService{
    HashMap<Long, Person> people = new HashMap<>();
    DataFeeder df = new DataFeeder();

    @Override
    public List<Person> findAllPeople() {
        return new ArrayList<>(people.values());
    }

    @Override
    public void addPerson(Person p) {
        people.put(p.getId(), p);
    }

    @Override
    public Person createPerson(String input) {
        Person p = df.convertStringToPerson(input);
        addPerson(p);
        return p;
    }

    @Override
    public void sort(List<Person> people, String sortBy) {
        df.sortBy(sortBy, people);
    }
}
