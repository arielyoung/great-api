package young.ariel.converter;

import young.ariel.model.Person;
import young.ariel.model.PersonDto;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PersonConverter {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public PersonDto convertToDto(Person p) {
        PersonDto personDto = new PersonDto();
        personDto.setDateOfBirth(formatter.format(p.getDateOfBirth()));
        personDto.setFirstName(p.getFirstName());
        personDto.setLastName(p.getLastName());
        personDto.setGender(p.getGender());
        personDto.setFavoriteColor(p.getFavoriteColor());
        personDto.setId(p.getId());

        return personDto;
    }

    public Person createPerson(PersonDto p) {
        Person person = new Person();
        person.setFirstName(p.getFirstName());
        person.setLastName(p.getLastName());
        person.setFavoriteColor(p.getFavoriteColor());
        person.setGender(p.getGender());
        try {
            person.setDateOfBirth(formatter.parse(p.getDateOfBirth()));
        } catch (ParseException parseException) {
            throw new IllegalArgumentException("Unable to parse: " + p.getDateOfBirth());
        }
        return person;
    }

}
