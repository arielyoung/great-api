package young.ariel.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.util.UriComponentsBuilder;
import young.ariel.converter.PersonConverter;
import young.ariel.model.Person;
import young.ariel.model.PersonDto;
import young.ariel.service.PersonService;

@RestController
@RequestMapping("/api")
public class RestApiController {

    @Autowired
    PersonService personService; //Service which will do all data retrieval/manipulation work

    PersonConverter converter = new PersonConverter();

    @RequestMapping(value = "/records", method = RequestMethod.GET)
    public ResponseEntity<List<PersonDto>> listAllUsers() {
        List<PersonDto> people = personService.findAllPeople().stream().map(p -> converter.convertToDto(p)).collect(Collectors.toList());
        if (people.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(people, HttpStatus.OK);
    }

    @RequestMapping(value = "/records", method = RequestMethod.POST)
    public ResponseEntity<PersonDto> createPerson(@RequestBody String input, UriComponentsBuilder ucBuilder) {
        Person newPerson = personService.createPerson(input);
        PersonDto personDto = converter.convertToDto(newPerson);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/user/{id}").buildAndExpand(newPerson.getId()).toUri());
        return new ResponseEntity<>(personDto, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/records/{sort}", method = RequestMethod.GET)
    public ResponseEntity<List<PersonDto>> sort(@PathVariable String sort) {
        List<Person> people = personService.findAllPeople();
        personService.sort(people, sort);
        List<PersonDto> peopleDto = people.stream().map(p -> converter.convertToDto(p)).collect(Collectors.toList());;
        return new ResponseEntity<>(peopleDto, HttpStatus.OK);
    }
}