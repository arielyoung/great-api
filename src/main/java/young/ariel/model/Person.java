package young.ariel.model;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Person {
    private static final AtomicInteger ID_GENERATOR = new AtomicInteger(1000);

    private long id;
    private String lastName;
    private String firstName;
    private String gender;
    private String favoriteColor;
    private Date dateOfBirth;

    public Person() {
        this.id = ID_GENERATOR.getAndIncrement();
    }

    public Person(String lastName, String firstName, String gender, String favoriteColor, Date dateOfBirth) {
        this.id = ID_GENERATOR.getAndIncrement();
        this.lastName = lastName;
        this.firstName = firstName;
        this.gender = gender;
        this.favoriteColor = favoriteColor;
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFavoriteColor() {
        return favoriteColor;
    }

    public void setFavoriteColor(String favoriteColor) {
        this.favoriteColor = favoriteColor;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getId() {
        return id;
    }

    public String toString() {
        return "Last Name: " + lastName
                + "\tFirst Name: " + firstName
                + "\tGender: " + gender
                + "\tFavorite Color: " + favoriteColor
                + "\tDate of Birth: " + dateOfBirth;
    }
}
