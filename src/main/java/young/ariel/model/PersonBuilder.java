package young.ariel.model;

import java.util.Date;

public class PersonBuilder {
    private String lastName;
    private String firstName;
    private String gender;
    private String favoriteColor;
    private Date dateOfBirth;

    public String getLastName() {
        return lastName;
    }

    public PersonBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public PersonBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public PersonBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getFavoriteColor() {
        return favoriteColor;
    }

    public PersonBuilder setFavoriteColor(String favoriteColor) {
        this.favoriteColor = favoriteColor;
        return this;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public PersonBuilder setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public Person build() {
        return new Person(lastName, firstName, gender, favoriteColor, dateOfBirth);
    }
}
