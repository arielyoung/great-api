package young.ariel;

import young.ariel.model.Person;
import young.ariel.model.PersonBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataFeeder {
    SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");

    public static void main(String[] args) {
        DataFeeder df = new DataFeeder();
        for (String file: args) {
            List<Person> people = new ArrayList<>(df.readFile(file));
            df.outputBy("Gender", people);
            df.outputBy("Name", people);
            df.outputBy("Birthdate", people);
        }
    }

    public List<Person> readFile(String fileName) {
        List<Person> people = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = reader.readLine()) != null) {
                people.add(convertStringToPerson(line));
            }
            reader.close();
        } catch(Exception e) {
            System.out.println("Problem reading file: " + fileName);
            e.printStackTrace();
        }
        return people;
    }

    public Person convertStringToPerson(String line) {
        List<String> input = new ArrayList<>(Arrays.asList(line.split("\\||,|\\s+")));
        input.removeAll(Collections.singleton(""));
        if (input.size() != 5) {
            return null;
        }

        Person p = null;
        try {
            p = new PersonBuilder()
                    .setLastName(input.get(0))
                    .setFirstName(input.get(1))
                    .setGender(input.get(2))
                    .setFavoriteColor(input.get(3))
                    .setDateOfBirth(formatter.parse(input.get(4)))
                    .build();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return p;
    }

     public void sortBy(String method, List<Person> people) {
        if (method.equalsIgnoreCase("Name")) {
            people.sort(Comparator.comparing(Person::getLastName).reversed());
        } else if (method.equalsIgnoreCase("Birthdate")) {
            people.sort(Comparator.comparing(Person::getDateOfBirth));
        } else if (method.equalsIgnoreCase("Gender")) {
            people.sort(Comparator.comparing(Person::getGender).thenComparing(Person::getLastName));
        } else {
            throw new UnsupportedOperationException("Method to sort by not found: " + method);
        }

    }

    public void outputBy(String method, List<Person> people) {
        sortBy(method, people);
        System.out.println(String.format("----------Ordered By %s ----------", method));
        people.forEach(s -> System.out.println(s.toString()));
    }
}
